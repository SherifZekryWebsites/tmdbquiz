//
//  movieCellViewTableViewCell.swift
//  tmdbquiz
//
//  Created by Sherif  Wagih on 7/21/18.
//  Copyright © 2018 Sherif  Wagih. All rights reserved.
//

import UIKit

class movieCellViewTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var cellImageHeight: NSLayoutConstraint!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
