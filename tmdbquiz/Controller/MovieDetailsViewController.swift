//
//  MovieDetailsViewController.swift
//  tmdbquiz
//
//  Created by Sherif  Wagih on 7/27/18.
//  Copyright © 2018 Sherif  Wagih. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher
class MovieDetailsViewController: UIViewController {

    //the image link for the api
    var imageLink: String = "https://image.tmdb.org/t/p/w500";
    
    @IBOutlet weak var idButton: UILabel!
    
    @IBAction func close(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
   }
    
    var upcomingMovie : movie?
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false);
        titleLabel.text = upcomingMovie?.title;
      
        idButton.text = "\(upcomingMovie?.id ?? 0)"
        idButton.layer.masksToBounds = true

        idButton.layer.cornerRadius = 30

         self.navigationController?.navigationBar.layer.zPosition = -1;

        let imageURL =  "\(imageLink)\((upcomingMovie?.poster_path)!)"
        if let url = URL(string: imageURL) {
            ImageDownloader.default.downloadImage(with: url, options: [], progressBlock: nil) {
                (image, error, url, data) in
                DispatchQueue.main.async {
                   self.movieImage.image = image
                }
            }
        }
        movieImage.layer.cornerRadius = 12
        movieImage.clipsToBounds = true
        movieImage.layer.borderWidth = 2
        movieImage.layer.borderColor = UIColor.black.cgColor
        descriptionLabel.text = upcomingMovie?.overview;
     
        cosmosView.rating = ((upcomingMovie?.vote_average)! / 2);
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain
            , target: nil, action: nil)
        view.layoutIfNeeded();
    }

   
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cosmosView: CosmosView!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
