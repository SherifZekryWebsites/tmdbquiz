//
//  MoviesViewController.swift
//  tmdbquiz
//
//  Created by Sherif  Wagih on 7/21/18.
//  Copyright © 2018 Sherif  Wagih. All rights reserved.
//

import UIKit
import  SVProgressHUD
import Kingfisher
//import Alamofire
//import SwiftyJSON

class MoviesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate {
    
   

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true);
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    //array that holds all the now playing movies
    var allmovies:nowPlaying = nowPlaying();
    //array we use to search;
    var displayedallmovies:nowPlaying = nowPlaying();
    //the image link for the api
    var imageLink: String = "https://image.tmdb.org/t/p/w500";
    //api link
    var api_full_url = "https://api.themoviedb.org/3/movie/now_playing?api_key=b4c0e179c4be2385cfa06f73ae68dce9&language=en-US&page=1";
    //current selected index pat
    var selectedIndexPath:IndexPath?;
    //outlets for the table and searchtextfield
    @IBOutlet weak var tableViewReference: UITableView!
    @IBOutlet weak var searchTextField: UISearchBar!
   //get all the data
    func getMoviesData(url: URL)
    {
        SVProgressHUD.show();
        //print("we are here");
        URLSession.shared.dataTask(with: url){
            (data,response,error) in
            
            if error != nil {
                print(error!)
                return
            }
            else
            {
                let httpResponse = response as! HTTPURLResponse
                if httpResponse.statusCode == 200{
                    //print("correct response");
                    guard let data = data else { print("no data") ; return;}
                    
                    do
                    {
                        //print("we are here")
                        self.allmovies = try JSONDecoder().decode(nowPlaying.self, from: data);
                        self.displayedallmovies.results = self.allmovies.results
                        self.tableViewReference.performSelector(onMainThread: #selector(UICollectionView.reloadData), with: nil, waitUntilDone: true)
                        // print(self.allmovies?.results)
                    }
                    catch let jsonError
                    {
                        print("here is the error\(jsonError)");
                    }
                }
                else
                {
                    print("wrong response");
                }
            }
            }.resume();
      
        SVProgressHUD.dismiss();
    }
    //conform to the two protocols of tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
      return displayedallmovies.results.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        selectedIndexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        performSegue(withIdentifier: "showMovieDetails", sender: self)
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieDetails"
        {
            let movieDetails: MovieDetailsViewController = segue.destination as! MovieDetailsViewController
            movieDetails.upcomingMovie = displayedallmovies.results[(selectedIndexPath?.row)!];
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableViewReference.dequeueReusableCell(withIdentifier: "movieCellSeparated", for: indexPath) as! movieCellViewTableViewCell;
        cell.titleLabel.text = displayedallmovies.results[indexPath.row].title;
        cell.descriptionLabel.text = displayedallmovies.results[indexPath.row].overview;
       
        
        let imageURL =  "\(imageLink)\((displayedallmovies.results[indexPath.row].poster_path)!)";
        if let url = URL(string: imageURL) {
            ImageDownloader.default.downloadImage(with: url, options: [], progressBlock: nil) {
                (image, error, url, data) in
                DispatchQueue.main.async {
                    cell.posterImage.image = image
                }
            }
        }
        /*var image: UIImage?
            if let url = imageURL {
                //All network operations has to run on different thread(not on main thread).
                DispatchQueue.global(qos: .userInitiated).async {
                    let imageData = NSData(contentsOf: url)
                    //All UI operations has to run on main thread.
                    DispatchQueue.main.async {
                        if imageData != nil {
                            image = UIImage(data: imageData as! Data)
                             cell.posterImage.image = image
                           
                        } else {
                            image = nil
                        }
                    }
                }
        }*/
      /*  cell.posterImage.downloadedFrom(link: "\(imageLink)\((displayedallmovies.results[indexPath.row].poster_path)!)")
      */
        
        return cell;
        
    }
    //setting a fixed height to the row
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad();
        guard  let url = URL(string: api_full_url) else { print("false");return }
        getMoviesData(url: url);
        tableViewReference.dataSource = self;
        tableViewReference.delegate = self;
        tableViewReference.register(UINib(nibName: "movieCellViewTableViewCell", bundle: nil), forCellReuseIdentifier: "movieCellSeparated")
        setupSearchBar();
        
    }
    
   
    //implementing searchbar functions
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        SVProgressHUD.show();
        print(displayedallmovies.results.count)
        print(allmovies.results.count)
        guard !searchText.isEmpty
            else {displayedallmovies.results = allmovies.results;tableViewReference.reloadData();SVProgressHUD.dismiss(); return;};
        
        displayedallmovies.results = (allmovies.results).filter({
            return $0.title!.contains(searchText)
        })
        
        tableViewReference.reloadData();
        SVProgressHUD.dismiss();
    }
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
    }
    func setupSearchBar()
    {
        searchTextField.delegate = self;
    }
    
    
    
    

}


