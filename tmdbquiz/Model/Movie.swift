//
//  Movie.swift
//  tmdbquiz
//
//  Created by Sherif  Wagih on 7/21/18.
//  Copyright © 2018 Sherif  Wagih. All rights reserved.
//

import Foundation

class nowPlaying: Decodable
{
    var results: [movie] = [];
}
class movie: Decodable
{
    let id : Int?;
    let title: String?;
    let overview: String?;
    let poster_path: String?;
    let vote_average: Double?;
}
